package com.test.trips.extentions

import android.util.Log
import com.test.trips.model.system.SchedulersProvider
import io.reactivex.*

const val LOG_TAG = "LogTours"

fun <T> Single<T>.withDefaults(schedulers: SchedulersProvider): Single<T> {
    return compose {
        it.subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnError { error -> Log.e(LOG_TAG, error.message) }
    }
}

fun <T> Flowable<T>.withDefaults(schedulers: SchedulersProvider): Flowable<T> {
    return compose {
        it.subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnError { error -> Log.e(LOG_TAG, error.message) }
    }
}

fun <T> Maybe<T>.withDefaults(schedulers: SchedulersProvider): Maybe<T> {
    return compose {
        it.subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnError { error -> Log.e(LOG_TAG, error.message) }
    }
}

fun <T> Observable<T>.withDefaults(schedulers: SchedulersProvider): Observable<T> {
    return compose {
        it.subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnError { error -> Log.e(LOG_TAG, error.message) }
    }
}

fun Completable.withDefaults(schedulers: SchedulersProvider): Completable {
    return compose {
        it.subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnError { error -> Log.e(LOG_TAG, error.message) }
    }
}
