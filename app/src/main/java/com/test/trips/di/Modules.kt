package com.test.trips.di

import com.test.trips.model.local.db.TourDatabase
import com.test.trips.model.local.TourLocalDataSource
import com.test.trips.model.network.ToursApi
import com.test.trips.model.network.createNetworkClient
import com.test.trips.model.remote.TourRemoteDataSource
import com.test.trips.model.repositories.TourRepository
import com.test.trips.model.system.AppSchedulers
import com.test.trips.viewmodel.TourViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

val viewModelModule: Module = module {
    viewModel { TourViewModel(tourRepository = get()) }
}

val repositoryModule = module {
    single { TourRepository(localDataSource = get(), remoteDataSource = get(), schedulers = get()) }
}

val localDataSourceModule = module {
    single { TourLocalDataSource(tourDatabase = get()) }
}

val remoteDataSourceModule = module {
    single { TourRemoteDataSource(toursApi = get()) }
}

val networkModule = module {
    single { create<ToursApi>() }
}

val databaseModule = module {
    single { TourDatabase.getInstance(context = androidContext()) }
}

val rxModule = module {
    single { AppSchedulers() }
}

inline fun <reified T> create(): T = createNetworkClient().create(T::class.java)