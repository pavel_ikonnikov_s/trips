package com.test.trips.model.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.test.trips.model.local.entity.Tour

@Dao
interface TourDao : BaseDao<Tour> {

    @Query("SELECT * FROM Tour ")
    fun getAllTours(): LiveData<MutableList<Tour>>

}