package com.test.trips.model.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Tour(
    @PrimaryKey
    val id: Int,
    val hotelName: String,
    val hotelPrice: Int,
    val flights: List<Flight>
)