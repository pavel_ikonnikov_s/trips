package com.test.trips.model.local.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.test.trips.model.local.dao.TourDao
import com.test.trips.model.local.entity.Tour

@Database(
    entities = [
        Tour::class
    ], version = 1
)
@TypeConverters(RoomTypeConverters::class)
abstract class TourDatabase : RoomDatabase() {

    abstract fun tourDao(): TourDao

    companion object {
        @Volatile
        private var INSTANCE: TourDatabase? = null
        private const val DATABASE_NAME = "TourDatabase.db"

        fun getInstance(context: Context): TourDatabase? {
            if (INSTANCE == null) {
                synchronized(TourDatabase::class.java) {
                    if (INSTANCE == null) {
                        val builder = Room
                            .databaseBuilder(context.applicationContext, TourDatabase::class.java,
                                DATABASE_NAME
                            )

                        INSTANCE = builder.build()
                    }
                }
            }
            return INSTANCE
        }
    }
}