package com.test.trips.model.remote.data

import com.google.gson.annotations.SerializedName

data class CompanyRemote(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String
)