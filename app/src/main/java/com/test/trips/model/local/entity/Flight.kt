package com.test.trips.model.local.entity

data class Flight (
    val companyName: String,
    val price: Int
)