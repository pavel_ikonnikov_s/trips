package com.test.trips.model.network

import com.test.trips.model.remote.data.CompanyResponseData
import com.test.trips.model.remote.data.FlightResponseData
import com.test.trips.model.remote.data.HotelResponseData
import io.reactivex.Single
import retrofit2.http.GET


interface ToursApi {

    @GET("bins/zqxvw")
    fun getFlights(): Single<FlightResponseData>

    @GET("bins/12q3ws")
    fun getHotels(): Single<HotelResponseData>

    @GET("bins/8d024")
    fun getCompanies(): Single<CompanyResponseData>


}