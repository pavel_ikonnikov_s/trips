package com.test.trips.model.repositories

import androidx.lifecycle.LiveData
import com.test.trips.extentions.withDefaults
import com.test.trips.model.local.TourLocalDataSource
import com.test.trips.model.local.entity.Flight
import com.test.trips.model.local.entity.Tour
import com.test.trips.model.remote.TourRemoteDataSource
import com.test.trips.model.remote.data.CompanyRemote
import com.test.trips.model.remote.data.FlightRemote
import com.test.trips.model.remote.data.HotelRemote
import com.test.trips.model.system.AppSchedulers
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.functions.Function3


class TourRepository(
    private val localDataSource: TourLocalDataSource,
    private val remoteDataSource: TourRemoteDataSource,
    private val schedulers: AppSchedulers
) {

    fun getTours(): LiveData<MutableList<Tour>> {
        return localDataSource.getTours()
    }

    fun loadTours(): Single<MutableList<Tour>> {
        return Single.zip(
            loadHotels(),
            loadFlights(),
            loadCompanies(),
            Function3 { hotels, flights, companies ->
                hotels
                    .asSequence()
                    .map { hotel ->
                        Tour(
                            id = hotel.id,
                            hotelName = hotel.name,
                            hotelPrice = hotel.price,
                            flights = hotel.flights
                                .asSequence()
                                .map { flightId ->
                                    flights.first { it.id == flightId }
                                }.map { flight ->
                                    Flight(
                                        companyName = companies.first { it.id == flight.companyId }.name,
                                        price = flight.price
                                    )
                                }
                                .toList()
                        )
                    }.toMutableList()
            }
        )
    }

    fun saveTours(tours: MutableList<Tour>): Completable {
        return localDataSource.saveTours(tours)
            .withDefaults(schedulers)
    }

    private fun loadHotels(): Single<MutableList<HotelRemote>> {
        return remoteDataSource.loadHotels()
            .map { it.hotels }
            .withDefaults(schedulers)
    }

    private fun loadCompanies(): Single<MutableList<CompanyRemote>> {
        return remoteDataSource.loadCompanies()
            .map { it.companies }
            .withDefaults(schedulers)
    }

    private fun loadFlights(): Single<MutableList<FlightRemote>> {
        return remoteDataSource.loadFlights()
            .map { it.flights }
            .withDefaults(schedulers)
    }

}
