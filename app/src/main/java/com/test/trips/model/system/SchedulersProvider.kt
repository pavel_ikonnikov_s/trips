package com.test.trips.model.system

import io.reactivex.Scheduler

interface SchedulersProvider {
    fun ui(): Scheduler
    fun computation(): Scheduler
    fun newThread(): Scheduler
    fun io(): Scheduler
}
