package com.test.trips.model.local.db

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.test.trips.model.local.entity.Flight


class RoomTypeConverters {
    private var gson = Gson()

    @TypeConverter
    fun listOfFlightsToString(obj: List<Flight>?): String? = gson.toJson(obj)

    @TypeConverter
    fun stringToListOfFlights(data: String?): List<Flight>? {
        return gson.fromJson(data, object : TypeToken<List<Flight>>() {}.type)
    }
}