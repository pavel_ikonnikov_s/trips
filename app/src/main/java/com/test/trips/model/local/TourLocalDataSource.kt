package com.test.trips.model.local

import androidx.lifecycle.LiveData
import com.test.trips.model.local.db.TourDatabase
import com.test.trips.model.local.entity.Tour
import io.reactivex.Completable

class TourLocalDataSource(
    private val tourDatabase: TourDatabase
) {
    fun getTours(): LiveData<MutableList<Tour>> = tourDatabase.tourDao().getAllTours()

    fun saveTours(tours: MutableList<Tour>) :Completable = tourDatabase.tourDao().insertAll(tours)
}