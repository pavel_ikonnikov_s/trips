package com.test.trips.model.remote

import com.test.trips.model.network.ToursApi
import com.test.trips.model.remote.data.CompanyResponseData
import com.test.trips.model.remote.data.FlightResponseData
import com.test.trips.model.remote.data.HotelResponseData
import io.reactivex.Single


class TourRemoteDataSource (
    private val toursApi: ToursApi
){

    fun loadHotels(): Single<HotelResponseData> = toursApi.getHotels()

    fun loadCompanies(): Single<CompanyResponseData> = toursApi.getCompanies()

    fun loadFlights(): Single<FlightResponseData> = toursApi.getFlights()
}