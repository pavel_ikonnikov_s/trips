package com.test.trips.model.remote.data

import com.google.gson.annotations.SerializedName

data class FlightResponseData (
    @SerializedName("flights")
    val flights: MutableList<FlightRemote>
)