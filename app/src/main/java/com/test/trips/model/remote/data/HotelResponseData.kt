package com.test.trips.model.remote.data

import com.google.gson.annotations.SerializedName

data class HotelResponseData (
    @SerializedName("hotels")
    val hotels: MutableList<HotelRemote>
)