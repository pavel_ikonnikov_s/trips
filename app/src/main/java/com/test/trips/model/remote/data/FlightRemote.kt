package com.test.trips.model.remote.data

import com.google.gson.annotations.SerializedName


data class FlightRemote(
    @SerializedName("id")
    val id: String,
    @SerializedName("companyId")
    val companyId: Int,
    @SerializedName("departure")
    val departure: String,
    @SerializedName("arrival")
    val arrival: String,
    @SerializedName("price")
    val price: Int
)
