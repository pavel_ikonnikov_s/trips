package com.test.trips.model.remote.data

import com.google.gson.annotations.SerializedName

data class HotelRemote(
    @SerializedName("id")
    val id: Int,
    @SerializedName("flights")
    val flights: List<String>,
    @SerializedName("name")
    val name: String,
    @SerializedName("price")
    val price: Int
)
