package com.test.trips.model.remote.data

import com.google.gson.annotations.SerializedName

data class CompanyResponseData (
    @SerializedName("companies")
    val companies: MutableList<CompanyRemote>
)