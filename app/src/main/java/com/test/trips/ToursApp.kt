package com.test.trips

import android.app.Application
import com.test.trips.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin


class ToursApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@ToursApp)
            androidLogger()
            modules(
                viewModelModule,
                repositoryModule,
                localDataSourceModule,
                remoteDataSourceModule,
                networkModule,
                databaseModule,
                rxModule
            )
        }
    }

}