package com.test.trips.view

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.customListAdapter
import com.test.trips.R
import com.test.trips.adapters.DialogFlightAdapter
import com.test.trips.adapters.ToursAdapter
import com.test.trips.viewmodel.TourViewModel
import kotlinx.android.synthetic.main.activity_tours.*
import org.koin.androidx.viewmodel.ext.viewModel

class TourActivity : AppCompatActivity() {
    private val viewModel: TourViewModel by viewModel()
    private val toursAdapter by lazy { ToursAdapter(itemClick = { onItemClick(it) }) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tours)

        initView()
        viewModel.tours.observe(this, Observer { tours -> toursAdapter.submitList(tours) })
    }

    private fun initView() {
        recycler.adapter = toursAdapter
        swipeRefreshLayout.setOnRefreshListener {
            swipeRefreshLayout.isRefreshing = false
            viewModel.loadTours()
        }
    }

    private fun onItemClick(flightInfo: MutableList<Pair<Int, String>>) {
        val flightAdapter = DialogFlightAdapter(flightInfo)
        MaterialDialog(this).show {
            customListAdapter(flightAdapter)
            positiveButton(R.string.dialog_accept_button) {
                Toast.makeText(
                    context, String.format(
                        resources.getString(
                            R.string.dialog_accept_button_chosen_tour
                        ),
                        flightInfo[flightAdapter.selectedItem].second,
                        flightInfo[flightAdapter.selectedItem].first
                    ),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }
}
