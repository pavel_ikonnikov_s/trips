package com.test.trips.adapters


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.test.trips.R
import com.test.trips.model.local.entity.Tour
import kotlinx.android.synthetic.main.layout_tour_item.view.*


class ToursAdapter(
    private val itemClick: (MutableList<Pair<Int, String>>) -> Unit
) : ListAdapter<Tour, ToursAdapter.ViewHolder>(ToursDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_tour_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(getItem(position))

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(tour: Tour) {
            with(itemView) {
                val flights = tour.flights.size
                val tourPrice = (tour.hotelPrice + (tour.flights.minBy { it.price }?.price ?: 0))
                setOnClickListener { itemClick.invoke(tour.flights.map { Pair(it.price + tour.hotelPrice, it.companyName) }.toMutableList()) }
                textHotelName.text = tour.hotelName
                textToursCount.text = resources.getQuantityString(R.plurals.flight_options, flights, flights)
                textPrice.text = when (flights) {
                    1 -> String.format(resources.getString(R.string.tour_price), tourPrice)
                    else -> String.format(resources.getString(R.string.tour_price_from), tourPrice)
                }
            }
        }
    }

    private class ToursDiffCallback : DiffUtil.ItemCallback<Tour>() {
        override fun areItemsTheSame(oldItem: Tour, newItem: Tour): Boolean = oldItem.id == newItem.id
        override fun areContentsTheSame(oldItem: Tour, newItem: Tour): Boolean = oldItem == newItem
    }
}