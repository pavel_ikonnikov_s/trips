package com.test.trips.adapters


import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.CompoundButtonCompat
import androidx.recyclerview.widget.RecyclerView
import com.test.trips.R
import com.test.trips.extentions.getColorCompat
import kotlinx.android.synthetic.main.layout_tour_dialog_item.view.*


class DialogFlightAdapter(
    private var items: MutableList<Pair<Int, String>>
) : RecyclerView.Adapter<DialogFlightAdapter.ViewHolder>() {
    var selectedItem = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_tour_dialog_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position], position)

    override fun getItemCount(): Int = items.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(flightInfo: Pair<Int, String>, position: Int) {
            val clickListener = View.OnClickListener {
                selectedItem = adapterPosition
                notifyDataSetChanged()
            }
            with(itemView) {
                with(radioButton){
                    val states = arrayOf(intArrayOf(android.R.attr.state_checked), intArrayOf())
                    val colors = intArrayOf(
                        context.getColorCompat(if (position == selectedItem) R.color.colorAccent else R.color.colorTextSecondary),
                        context.getColorCompat(R.color.colorTextSecondary))
                    CompoundButtonCompat.setButtonTintList(this, ColorStateList(states, colors))
                    isChecked = position == selectedItem
                    text = flightInfo.second
                    setOnClickListener (clickListener)
                }
                textPrice.text = String.format(resources.getString(R.string.tour_price), flightInfo.first)
                setOnClickListener (clickListener)
            }
        }
    }
}