package com.test.trips.viewmodel

import androidx.lifecycle.LiveData
import com.test.trips.model.local.entity.Tour
import com.test.trips.model.repositories.TourRepository

class TourViewModel(
    private val tourRepository: TourRepository
) : BaseViewModel() {

    init {
        loadTours()
    }

    val tours: LiveData<MutableList<Tour>> = tourRepository.getTours()

    fun loadTours() {
        tourRepository.loadTours()
            .compositeSubscribe(
                onSuccess = { tours -> saveTours(tours) }
            )
    }

    private fun saveTours(tours: MutableList<Tour>) {
        tourRepository.saveTours(tours)
            .compositeSubscribe()
    }

}