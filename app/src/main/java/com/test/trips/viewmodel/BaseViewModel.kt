package com.test.trips.viewmodel

import androidx.lifecycle.ViewModel
import io.reactivex.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel : ViewModel() {
    private val compositeDisposable by lazy { CompositeDisposable() }

    protected fun Completable.compositeSubscribe(
        onSuccess: () -> Unit = {},
        onError: (Throwable) -> Unit = {}
    ) = subscribe(onSuccess, onError)
        .composite()

    protected fun <T : Any> Flowable<T>.compositeSubscribe(
        onNext: (T) -> Unit = {},
        onError: (Throwable) -> Unit = {},
        onComplete: () -> Unit = {}
    ) = subscribe(onNext, onError, onComplete)
        .composite()

    protected fun <T : Any> Single<T>.compositeSubscribe(
        onSuccess: (T) -> Unit = {},
        onError: (Throwable) -> Unit = {}
    ) = subscribe(onSuccess, onError)
        .composite()

    protected fun <T : Any> Maybe<T>.compositeSubscribe(
        onSuccess: (T) -> Unit = {},
        onError: (Throwable) -> Unit = {}
    ) = subscribe(onSuccess, onError)
        .composite()

    protected fun <T : Any> Observable<T>.compositeSubscribe(
        onNext: (T) -> Unit = {},
        onError: (Throwable) -> Unit = {},
        onComplete: () -> Unit = {}
    ) = subscribe(onNext, onError, onComplete)
        .composite()

    private fun Disposable.composite() {
        compositeDisposable.add(this)
    }

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }
}